package org.udg.caes.signin.userLogin.state;
import mockit.Mock;
import mockit.MockUp;
import org.junit.Test;
import org.udg.caes.signin.userAccount.UserAccount;
import org.udg.caes.signin.userLogin.*;

import static mockit.Deencapsulation.getField;
import static org.junit.Assert.assertEquals;

/**
 * Created by Xavier Baubes on 7/11/14.
 */

// API state-based (MockUp<>)

public class TestSigninService
{

    //signin

    //usuari retorna null
    @Test(expected=UserAccountNotFoundException.class)
    public void testSigninUserNotFound() throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException
    {
        new MockUp<UserAccount>()
        {
            @Mock public UserAccount find(String id) {return null;}
        };

        SigninService s = new SigninService();
        s.signin("id","pass");
    }

    //registerNewLogin -> isSignedIn = true
    @Test(expected=AccountLoginLimitReachedException.class)
    public void testSigninLoginLimitReached() throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException
    {
        new MockUp<UserAccount>()
        {
            @Mock public UserAccount find(String id) { return new UserAccount(id,"pass"); }
            @Mock public boolean passwordMatches(String anyString) {return true;} //anyString -> li passem un string qualsevol
            @Mock public boolean isSignedIn() {return true;}
        };

        SigninService s = new SigninService();
        s.signin("id","pass");
    }

    //registerNewLogin -> isRevoked = true
    @Test(expected=UserAccountRevokedException.class)
    public void testSigninRevoked() throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException
    {
        new MockUp<UserAccount>()
        {
            @Mock public UserAccount find(String id) { return new UserAccount(id,"pass"); }
            @Mock public boolean passwordMatches(String n) {return true;}
            @Mock public boolean isSignedIn() {return false;}
            @Mock public boolean isRevoked() {return true;}
        };

        SigninService s = new SigninService();
        s.signin("id","pass");
    }

    //registerNewLogin -> execucio correcte
    @Test
    public void testSigninCorrect() throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException
    {
        new MockUp<UserAccount>()
        {
            @Mock public UserAccount find(String id) {
                //comprovem que el parametre id es passa correctament
                assertEquals("id", id);
                return new UserAccount(id, "pass");
            }
            @Mock public boolean passwordMatches(String n){
                //comprovem que el parametre n es passa correctament
                assertEquals("pass", n);
                return true;
            }
            @Mock public boolean isSignedIn() {return false;}
            @Mock public boolean isRevoked() {return false;}

            //verfifiquem l'execucio
            @Mock(invocations = 1) void setSignedIn(boolean b)
            {
                assertEquals("El valor del parametre b de setSignedIn hauria de ser true",b,true);
            }
        };

        SigninService s = new SigninService();
        s.signin("id","pass");
    }

    //handleFailedLoginAttempt -> passwordMatches = false
    @Test
    public void testSigninFailPass() throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException
    {
        new MockUp<UserAccount>()
        {
            @Mock public UserAccount find(String id) { return new UserAccount(id,"pass"); }
            @Mock public boolean passwordMatches(String n) {return false;}

            @Mock(invocations = 0) void setRevoked(boolean b)
            {
                assertEquals(b,true);
            }
        };

        SigninService s = new SigninService();
        s.signin("id","pass");
    }

    //handleFailedLoginAttempt -> passwordMatches = false *3
    @Test
    public void testSignin3FailPass() throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException
    {
        new MockUp<UserAccount>()
        {
            @Mock public UserAccount find(String id) { return new UserAccount(id,"pass"); }
            @Mock public boolean passwordMatches(String n) {return false;}

            @Mock(invocations = 1) void setRevoked(boolean b)
            {
                assertEquals("El valor del parametre b de setRevoked hauria de ser true",b,true);
            }
        };

        SigninService s = new SigninService();
        s.signin("id","pass");
        s.signin("id","pass");
        s.signin("id","pass");
    }

    //handleFailedLoginAttempt -> passwordMatches = false *3 (no consecutiu)
    @Test
    public void testSignin3FailPassNo() throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException
    {
        new MockUp<UserAccount>()
        {
            @Mock public UserAccount find(String id) { return new UserAccount(id,"pass"); }
            @Mock public boolean passwordMatches(String n) {return false;}

            @Mock(invocations = 0) void setRevoked(boolean b)
            {
                assertEquals(b,true);
            }
        };

        SigninService s = new SigninService();
        s.signin("id","pass");
        s.signin("id","pass");

        //comprovem que l'atribut privat loginAttemptsRemaining=1
        int result1 = getField(s,"loginAttemptsRemaining");
        assertEquals(1, result1);

        s.signin("id2","pass"); //canviem identificador
        s.signin("id","pass");

        //comprovem que l'atribut privat loginAttemptsRemaining=3
        int result2 = getField(s,"loginAttemptsRemaining");
        assertEquals(3, result2);
    }

    //deleteAccount

    //usuari retorna null
    @Test(expected=UserAccountNotFoundException.class)
    public void testDeleteUserNotFound() throws UserAccountNotFoundException, UserAccountRevokedException, UserAccountSignedInException
    {
        new MockUp<UserAccount>()
        {
            @Mock public UserAccount find(String id) { return null; }
        };

        SigninService s = new SigninService();
        s.deleteAccount("id");
    }

    //isRevoked = true
    @Test(expected=UserAccountRevokedException.class)
    public void testDeleteRevoked() throws UserAccountNotFoundException, UserAccountRevokedException, UserAccountSignedInException
    {
        new MockUp<UserAccount>()
        {
            @Mock public UserAccount find(String id) { return new UserAccount(id,"pass"); }
            @Mock public boolean isRevoked() {return true;}
        };

        SigninService s = new SigninService();
        s.deleteAccount("id");
    }

    //isSignedIn = true
    @Test(expected=UserAccountSignedInException.class)
    public void testDeleteSigned() throws UserAccountNotFoundException, UserAccountRevokedException, UserAccountSignedInException
    {

        new MockUp<UserAccount>()
        {
            @Mock public UserAccount find(String id) { return new UserAccount(id,"pass"); }
            @Mock public boolean isRevoked() {return false;}
            @Mock public boolean isSignedIn() {return true;}
        };

        SigninService s = new SigninService();
        s.deleteAccount("id");
    }

    //execucio correcte
    @Test
    public void testDeleteCorrect() throws UserAccountNotFoundException, UserAccountRevokedException, UserAccountSignedInException
    {
        new MockUp<UserAccount>()
        {
            @Mock public UserAccount find(String id) { return new UserAccount(id,"pass"); }
            @Mock public boolean isRevoked() {return false;}
            @Mock public boolean isSignedIn() {return false;}

            @Mock(invocations = 1) void delete(String id)
            {
                assertEquals("id", id);
            }
        };

        SigninService s = new SigninService();
        s.deleteAccount("id");
    }

}
