package org.caes.exercici1;

import static org.junit.Assert.assertEquals;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(JUnitParamsRunner.class)
public class TestMultiply
{
    @Test
    @Parameters({"5,4,20","15,-1,-15","-1,-15,15","10,10,100"}) //proves amb resultats positius i negatius
    public void TestMult(double num1, double num2, double res)
    {
        Calculator c = new Calculator();

        double res2 = c.multiply(num1,num2);

        assertEquals(num1+" * "+num2+" hauria de ser igual a "+res, res ,res2, 0.0);
    }

    //si el valor es superior a 999
    @Test(expected=RuntimeException.class)
    public void TestMultExcepcio()
    {
        Calculator c = new Calculator();

        double res2 = c.multiply(100,100);
    }

    @Test
    public void TestMultCommuntacio()
    {
        Calculator c = new Calculator();

        //double res = c.multiply(c.multiply(9.0, 8.0),5);
        double res1 = c.multiply(9.0, 8.0);
        double res2 = c.multiply(8.0, 9.0);

        assertEquals("9*8 hauria de ser igual a 8*9", res1, res2, 0.0);
    }

}
