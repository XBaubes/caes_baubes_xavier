package org.caes.exercici1;

public class Calculator {

  public double add(double v1, double v2)
  {
    return v1 + v2;
  }

  public double divide(double v1, double v2)
  {
    return v1 / v2;
  }
  
  public double sqrt(double v)
  {
    return Math.sqrt(v);
  }
    
  public double multiply(double v1, double v2)
  {
    double res = v1 * v2;
    if (res > 999)
      throw new RuntimeException("Overflow");
    return res;
  }

}
