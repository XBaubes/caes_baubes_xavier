package org.caes.exercici1;

import static org.junit.Assert.assertEquals;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(JUnitParamsRunner.class)
public class TestDivide
{

    @Test
    @Parameters({"12,4,3","12,-4,-3","6,10,0.6"}) //proves amb resultats positius i negatius
    public void testDividir(double num1, double num2, double res)
    {
        Calculator c = new Calculator();

        double res2 = c.divide(num1, num2);

        assertEquals(num1+" / "+num2+" hauria de ser igual a "+res, res, res2, 0.0);
    }

    @Test
    @Parameters({"22,3,7.333","-50,49,-1.020"})
    public void testDividirAprox(double num1, double num2, double res)
    {
        Calculator c = new Calculator();

        double res2 = c.divide(num1, num2);

        assertEquals(num1+" / "+num2+" hauria de ser igual a "+res, res, res2, 0.001);
    }

    @Test
    public void testInfinitPositiu()
    {
        Calculator c = new Calculator();

        double res = c.divide(8.0, 0.0);

        assertEquals("res", Double.POSITIVE_INFINITY, res, 0.0);
    }

    @Test
    public void testInfinitNegatiu()
    {
        Calculator c = new Calculator();

        double res = c.divide(-8.0, 0.0);

        assertEquals("res", Double.NEGATIVE_INFINITY, res, 0.0);
    }

}
