package org.caes.exercici1;

import static org.junit.Assert.assertEquals;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(JUnitParamsRunner.class)
public class TestSqrt
{

    @Test
    public void testArrelError()
    {
        Calculator c = new Calculator();

        double res = c.sqrt(-1.0);

        assertEquals("arrel de -1 hauria de ser igual a NaN", Double.NaN, res, 0.0);
    }

    @Test
    @Parameters({"9.0,3.0","100,10"})
    public void TestArrelParameters(double num1, double res)
    {
        Calculator c = new Calculator();

        double res2 = c.sqrt(num1);

        assertEquals("Arrel de "+num1+" hauria de ser igual a "+ res, res, res2, 0.0);
    }

    @Test
    @Parameters({"6.0,2.449","20,4.472"})
    public void TestArrelAproxParameters(double num1, double res)
    {
        Calculator c = new Calculator();

        double res2 = c.sqrt(num1);

        assertEquals("Arrel de "+num1+" hauria de ser igual a "+ res, res, res2, 0.001); //marge d'error per arrodoniment
    }

}
