package org.caes.exercici1;

import static org.junit.Assert.assertEquals;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(JUnitParamsRunner.class)
public class TestAdd
{
    @Test
    @Parameters({"5,4,9","15,1,16","5,-6,-1","-7,10,3"}) //proves amb resultats positius i negatius
    public void TestSumaParameters(double num1, double num2, double res)
    {
        Calculator c = new Calculator();

        double res2 = c.add(num1,num2);

        assertEquals(num1+" + "+num2+" hauria de ser igual a "+res, res ,res2, 0.0);
    }

    @Test
    public void testSumaInfinit()
    {
        Calculator c = new Calculator();

        double res = c.add(Double.POSITIVE_INFINITY,Double.POSITIVE_INFINITY);

        assertEquals("Suma d'infinits", res, Double.POSITIVE_INFINITY, 0.0);
    }

    @Test
    public void testSumaInfinitFora()
    {
        Calculator c = new Calculator();

        double res = c.add(Double.POSITIVE_INFINITY,Double.NEGATIVE_INFINITY);

        assertEquals("Resta d'infinits", res, Double.NaN, 0.0);
    }

    @Test
    public void testSumaMax()
    {
        Calculator c = new Calculator();

        double res = c.add(Double.MAX_VALUE,Double.MAX_VALUE);

        assertEquals("Suma de valors maxims", res, Double.POSITIVE_INFINITY, 0.0);
    }

    @Test
    public void testSumaMin()
    {
        Calculator c = new Calculator();

        double res = c.add(Double.MIN_VALUE,-Double.MIN_VALUE);

        assertEquals("Resta de valors minims", res, 0, 0.0);
    }

}
