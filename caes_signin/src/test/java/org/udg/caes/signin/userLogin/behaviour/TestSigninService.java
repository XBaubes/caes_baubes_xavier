package org.udg.caes.signin.userLogin.behaviour;
import mockit.*;
import org.junit.Test;
import org.udg.caes.signin.userAccount.UserAccount;
import org.udg.caes.signin.userLogin.*;

import static mockit.Deencapsulation.getField;
import static org.junit.Assert.assertEquals;

/**
 * Created by Xavier Baubes on 24/10/14.
 */

// API behaviour-based (Expectations & Verifications)

/*
new NonStrictExpectations(SigninService.class){{
}};
    //per les instancies de la classe SigninService
    //els metodes que no modifiquem es mantenent igual
*/

//a tots els que no hi ha @Test(expected=AccountLoginLimitReachedException.class) es fa verificacio de resultats

public class TestSigninService
{

    //signin

    //usuari retorna null
    @Test(expected=UserAccountNotFoundException.class) //indiquem l'excepcio esperada
    //aillem l'objecte UserAccount perque s'usa a SigninService
    public void testSigninUserNotFound(@Mocked final UserAccount user) throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException
    {
        /* ha diferencia de "NonStrictExpectations"
        tots els metodes que posem aqui dins d' "Expectations" s'han de cridar obligatoriament */
        new Expectations(){{
            UserAccount.find("id"); result = null;
        }};
        //NonStrictExpectations -> permet posar metodes que no es criden en l'execucio

        SigninService s = new SigninService();
        s.signin("id","pass");
    }

    //registerNewLogin -> isSignedIn = true
    @Test(expected=AccountLoginLimitReachedException.class)
    public void testSigninLoginLimitReached(@Mocked final UserAccount user) throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException
    {
        //dins d'aquest metode li diem a l'objecte mockejat els resultats que ens ha de retornar
        new NonStrictExpectations(){{
            UserAccount.find("id"); result = user; //com que el metode es static l'hem de cridar des de la classe no des d'una instancia de l'objecte
            user.passwordMatches("pass"); result = true; //... aqui si el podem crear des d'un objecte
            user.isSignedIn(); result = true;
        }};

        SigninService s = new SigninService();
        s.signin("id","pass");
    }

    //registerNewLogin -> isRevoked = true
    @Test(expected=UserAccountRevokedException.class)
    public void testSigninRevoked(@Mocked final UserAccount user) throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException
    {
        new NonStrictExpectations(){{
            UserAccount.find("id"); result = user;
            user.passwordMatches("pass"); result = true;
            user.isSignedIn(); result = false;
            user.isRevoked(); result = true;
        }};

        SigninService s = new SigninService();
        s.signin("id","pass");
    }

    //registerNewLogin -> execucio correcte
    @Test
    public void testSigninCorrect(@Mocked final UserAccount user) throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException
    {
        new NonStrictExpectations(){{
            UserAccount.find("id"); result = user;
            user.passwordMatches("pass"); result = true;
            user.isSignedIn(); result = false;
            user.isRevoked(); result = false;
        }};

        SigninService s = new SigninService();
        s.signin("id","pass");

        //verifiquem l'execucio:
        new Verifications(){{
            user.setSignedIn(true); times=1; //comprovem que ho fa
        }};
    }

    //handleFailedLoginAttempt -> passwordMatches = false
    @Test
    public void testSigninFailPass(@Mocked final UserAccount user) throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException
    {
        new NonStrictExpectations(){{
            UserAccount.find("id"); result = user;
            user.passwordMatches("pass"); result = false;
        }};

        SigninService s = new SigninService();
        s.signin("id","pass");

        //verifiquem l'execucio:
        new Verifications(){{
            user.setRevoked(true); times=0; //comprovem que no ho fa
        }};
    }

    //handleFailedLoginAttempt -> passwordMatches = false *3
    @Test
    public void testSignin3FailPass(@Mocked final UserAccount user) throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException
    {
        new NonStrictExpectations(){{ //new Expectations(){{
            UserAccount.find("id"); result = user;
            user.passwordMatches("pass"); result = false;
        }};

        SigninService s = new SigninService();
        s.signin("id","pass");
        s.signin("id","pass");
        s.signin("id","pass");

        //verifiquem l'execucio:
        new Verifications(){{
            user.setRevoked(true); times=1; //comprovem que ho fa una sola vegada
        }};
    }

    //handleFailedLoginAttempt -> passwordMatches = false *3 (no consecutiu)
    @Test
    public void testSignin3FailPassNo(@Mocked final UserAccount user) throws UserAccountNotFoundException, UserAccountRevokedException, AccountLoginLimitReachedException
    {
        new NonStrictExpectations(){{
            UserAccount.find("id"); result = user;
            user.passwordMatches("pass"); result = false;
        }};

        SigninService s = new SigninService();
        s.signin("id","pass");
        s.signin("id","pass");

        //comprovem que l'atribut privat loginAttemptsRemaining=1
        int result1 = getField(s,"loginAttemptsRemaining");
        assertEquals(1, result1);

        s.signin("id2","pass"); //canviem identificador
        s.signin("id","pass");

        //comprovem que l'atribut privat loginAttemptsRemaining=3
        int result2 = getField(s,"loginAttemptsRemaining");
        assertEquals(3, result2);

        //verifiquem l'execucio:
        new Verifications(){{
            user.setRevoked(true); times=0; //comprovem que no ho fa
        }};
    }

    //deleteAccount

    //usuari retorna null
    @Test(expected=UserAccountNotFoundException.class)
    public void testDeleteUserNotFound(@Mocked final UserAccount user) throws UserAccountNotFoundException, UserAccountRevokedException, UserAccountSignedInException
    {
        new NonStrictExpectations(){{
            UserAccount.find("id"); result = null;
        }};

        SigninService s = new SigninService();
        s.deleteAccount("id");
    }

    //isRevoked = true
    @Test(expected=UserAccountRevokedException.class)
    public void testDeleteRevoked(@Mocked final UserAccount user) throws UserAccountNotFoundException, UserAccountRevokedException, UserAccountSignedInException
    {
        new NonStrictExpectations(){{
            UserAccount.find("id"); result = user;
            user.isRevoked(); result = true;
        }};

        SigninService s = new SigninService();
        s.deleteAccount("id");
    }

    //isSignedIn = true
    @Test(expected=UserAccountSignedInException.class)
    public void testDeleteSigned(@Mocked final UserAccount user) throws UserAccountNotFoundException, UserAccountRevokedException, UserAccountSignedInException
    {
        new NonStrictExpectations(){{
            UserAccount.find("id"); result = user;
            user.isRevoked(); result = false;
            user.isSignedIn(); result = true;
        }};

        SigninService s = new SigninService();
        s.deleteAccount("id");
    }

    //execucio correcte (NonStrictExpectations + Verifications)
    @Test
    public void testDeleteCorrect(@Mocked final UserAccount user) throws UserAccountNotFoundException, UserAccountRevokedException, UserAccountSignedInException
    {
        new NonStrictExpectations(){{
            UserAccount.find("id"); result = user;
            user.isRevoked(); result = false;
            user.isSignedIn(); result = false;
        }};

        SigninService s = new SigninService();
        s.deleteAccount("id");

        new Verifications(){{
            UserAccount.delete("id"); times=1; //comprovem que ho fa
        }};
    }

    //testos alternatius per execucio correcte (deleteAccount):

    //execucio correcte (Expectations)
    @Test
    public void testDeleteCorrect2(@Mocked final UserAccount user) throws UserAccountNotFoundException, UserAccountRevokedException, UserAccountSignedInException
    {
        new Expectations(){{
            UserAccount.find("id"); result = user;
            user.isRevoked(); result = false;
            user.isSignedIn(); result = false;
            UserAccount.delete("id"); times=1; //Verifications
        }};

        SigninService s = new SigninService();
        s.deleteAccount("id");
    }

    //execucio correcte (NonStrictExpectations + FullVerifications)
    @Test
    public void testDeleteCorrect3(@Mocked final UserAccount user) throws UserAccountNotFoundException, UserAccountRevokedException, UserAccountSignedInException
    {
        new NonStrictExpectations(){{
            UserAccount.find("id"); result = user;
            user.isRevoked(); result = false;
            user.isSignedIn(); result = false;
        }};

        SigninService s = new SigninService();
        s.deleteAccount("id");

        new FullVerificationsInOrder(){{ //li posem tots els metodes que usem de l'objecte mockejat i comprova que els executa en ordre
            UserAccount.find("id");
            user.isRevoked();
            user.isSignedIn();
            UserAccount.delete("id"); times=1; //comprovem que ho fa
        }};
    }

}

